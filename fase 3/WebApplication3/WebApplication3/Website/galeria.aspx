﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="galeria.aspx.cs" Inherits="WebApplication3.Website.galeria" %>

<!DOCTYPE html>

<html lang="en-us">
<head>
 <meta charset = "UTF-8">
    
	<link rel="stylesheet" href="../css/screen.css">
	<link rel="stylesheet" href="../css/lightbox.css">
    <style type="text/css">

         html {
            background: url(/img/gt2.jpg); 
            width: 100%;
            height: 100%;
         }
         body{
            margin-top: 20px;
        }

        #header {
				margin:auto;
				width: 1050px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 40px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}

        h1{
            text-align: center;
        }

        .auto-style2 {
            text-decoration: none;
        }
        .auto-style3 {
            font-size: large;
        }

.galeria {
  height: calc( var(--h) + 3em);
  width: var(--w);
  margin:1em;
  border: 1px solid #555;
  position: relative;
  display:inline-block;
}

.galeria img {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
  transition: opacity 3s;
}

.galeria input[type=radio] {
  position: relative;
  bottom: calc(-1 * var(--h) - 1.5em);
  left: .5em;
}

.galeria input[type=radio]:nth-of-type(1):checked ~ img:nth-of-type(1) {
  opacity: 1;
}

.galeria input[type=radio]:nth-of-type(2):checked ~ img:nth-of-type(2) {
  opacity: 1;
}

.galeria input[type=radio]:nth-of-type(3):checked ~ img:nth-of-type(3) {
  opacity: 1;
}

.galeria input[type=radio]:nth-of-type(4):checked ~ img:nth-of-type(4) {
  opacity: 1;
}
.galeria input[type=button]:nth-of-type(5):checked ~ img:nth-of-type(5) {
  opacity: 1;
}

#t1{
    width: 100px;
    height: 100px;
}

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="header">
			<ul class="nav">
				<li><a href="inicio.aspx">Inicio</a></li>
                <li><a href="">Lugares de Turismo</a></li>
                <li><a href="Admin/admin.aspx">Administrador</a></li>
                <li><a href="">Empleados</a>
                    <ul>
						<li><a href="tecnico/tecnico.aspx">Técnico</a></li>
						<li><a href="agente/agente.aspx">Agente Turístico</a></li>
					</ul>
                </li>
				<li><a href="">Formularios</a>
					<ul>
						<li><a href="restaurantes.aspx">Restaurantes</a></li>
						<li><a href="hoteles.aspx">Hoteles</a></li>
						<li><a href="museos.aspx">Museos</a></li>
					</ul>
				</li>

				<li><a href="inicio.aspx">Acerca de</a></li>
			</ul>
		</div>


	<section id="examples" class="examples-section">
		<div class="container">
			<h2>Panajachel</h2>

			<div class="galeria" style="--w: 360px; --h: 200px;">
                <input type="radio" name="navigation1" id="_1" checked>
                <input type="radio" name="navigation1" id="_2">
                <input type="radio" name="navigation1" id="_3">
                <input type="radio" name="navigation1" id="_4"> 
                <asp:Button id="_5" runat="server" Text="Me Gusta" />

                <img src="../img/1.jpg" width="360" height="200" alt="Galeria CSS 1" />
                <img src="../img/2.jpg" width="360" height="200" alt="Galeria CSS 2"  />
                <img src="../img/3.jpg" width="360" height="200" alt="Galeria CSS 3" />
                <img src="../img/4.jpg" width="360" height="200" alt="Galeria CSS 4" />
            </div>
             <p id="t1">dewdew</p>
		</div>
	</section>
	 

       

	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/lightbox.js"></script>
	
        
    </form>


	</body>
</html>
