﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="ver-agentes.aspx.cs" Inherits="WebApplication3.Website.Admin.ver_agentes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

         html {
            background: url(/img/gt2.jpg); 
            width: 100%;
            height: 100%;
         }
         body{
            margin-top: 20px;
        }

			
			#header {
				margin:auto;
				width:750px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 50px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
			

        h1{
            text-align: center;
        }
        #tabla{
            text-align: center;
            
        }

        </style>
</head>
<body>
    <form id="form1" runat="server">
         <div id="header">
			<ul class="nav">
				<li><a href="">Inicio</a></li>
				<li><a href="">Tu Sesión</a>
					<ul>
						<li><a href="">Empresas solicitantes</a></li>
						<li><a href="">Empresas aceptadas</a></li>
						<li><a >Agregar</a>
							<ul>
								<li><a href="agregar-agente.aspx">Agente Turístico</a></li>
								<li><a href="agregar-admin.aspx">Administrador</a></li>
								<li><a href="agregar-tecnico.aspx">Técnico</a></li>
							</ul>
						</li>
                        <li><a >Ver</a>
							<ul>
								<li><a href="ver-agentes.aspx">Agentes Turísticos</a></li>
								<li><a href="ver-admin.aspx">Administradores</a></li>
								<li><a href="ver-tecnicos.aspx">Técnicos</a></li>
							</ul>
						</li>

					</ul>
				</li>
				<li><a href="">Acerca de</a></li>
				<li><a href="../inicio.aspx">Cerrar Sesión</a></li>
			</ul>
		</div>
        <br />
        <br />
        <br />

    <h1>Agentes Turísticos Activos</h1>
         <p>&nbsp;</p>
         <p id="tabla">
             <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                 <AlternatingRowStyle BackColor="White" />
                 <FooterStyle BackColor="#CCCC99" />
                 <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                 <RowStyle BackColor="#F7F7DE" />
                 <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                 <SortedAscendingCellStyle BackColor="#FBFBF2" />
                 <SortedAscendingHeaderStyle BackColor="#848384" />
                 <SortedDescendingCellStyle BackColor="#EAEAD3" />
                 <SortedDescendingHeaderStyle BackColor="#575357" />
             </asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:fase2ConnectionString %>" ProviderName="<%$ ConnectionStrings:fase2ConnectionString.ProviderName %>" SelectCommand="SELECT agente.* FROM agente"></asp:SqlDataSource>
         </p>

         
    </form>

    </body>
</html>
