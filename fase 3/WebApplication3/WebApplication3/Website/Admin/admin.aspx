﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="WebApplication3.Website.inicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

         html {
            background: url(/img/gt2.jpg); 
            width: 100%;
            height: 100%;
         }
         body{
            margin-top: 20px;
        }

			
			#header {
				margin:auto;
				width:750px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 50px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
			

        h1{
            text-align: center;
        }

        .auto-style2 {
            text-decoration: none;
        }
        .auto-style3 {
            font-size: large;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="header">
			<ul class="nav">
				<li><a href="">Inicio</a></li>
				<li><a href="">Tu Sesión</a>
					<ul>
						<li><a href="">Empresas solicitantes</a></li>
						<li><a href="">Empresas aceptadas</a></li>
						<li><a >Agregar</a>
							<ul>
								<li><a href="agregar-agente.aspx">Agente Turístico</a></li>
								<li><a href="agregar-admin.aspx">Administrador</a></li>
								<li><a href="agregar-tecnico.aspx">Técnico</a></li>
							</ul>
						</li>
                        <li><a >Ver</a>
							<ul>
								<li><a href="ver-agentes.aspx">Agentes Turísticos</a></li>
								<li><a href="ver-admin.aspx">Administradores</a></li>
								<li><a href="ver-tecnicos.aspx">Técnicos</a></li>
							</ul>
						</li>

					</ul>
				</li>
				<li><a href="">Acerca de</a></li>
				<li><a href="../inicio.aspx">Cerrar Sesión</a></li>
			</ul>
		</div>
    </form>
    <br />

    <h1>&nbsp;</h1>
    <h1>Instituto de Turismo Nacional</h1>
    <p>
        &nbsp;</p>
    <p class="MsoNormal">
        <span class="auto-style3">Guatemala está situada dentro del área geográfica conocida como&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Mesoamérica" title="Mesoamérica"><span class="auto-style3" style="color: windowtext; text-underline: none">Mesoamérica</span></a><span class="auto-style3">. Dentro de sus límites territoriales se desarrollaron varias culturas. Entre ellas la&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Civilización_Maya" title="Civilización Maya"><span class="auto-style3" style="color: windowtext; text-underline: none">Civilización Maya</span></a><span class="auto-style3">&nbsp;que fue notable por lograr un complejo desarrollo social. Sobresalió en varias disciplinas científicas tales como la arquitectura, la escritura, un avanzado cálculo del tiempo por medio de las matemáticas y la astronomía. El&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Calendario_maya" title="Calendario maya"><span class="auto-style3" style="color: windowtext; text-underline: none">calendario 
        maya</span></a><span class="auto-style3">&nbsp;que según los historiadores, era más preciso que el&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Calendario_gregoriano" title="Calendario gregoriano"><span class="auto-style3" style="color: windowtext; text-underline: none">calendario gregoriano</span></a><span class="auto-style3">&nbsp;que utilizamos hoy en día. Eran cazadores, agricultores, practicaban la&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Pesca" title="Pesca"><span class="auto-style3" style="color: windowtext; text-underline: none">pesca</span></a><span class="auto-style3">, domesticaban animales como pavos y patos; se trasportaban en canoas para navegar por los ríos y para viajar a las islas cercanas. También destacaron en la&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Pintura" title="Pintura"><span class="auto-style3" style="color: windowtext; text-underline: none">pintura</span></a><span class="auto-style3">, la&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Escultura" title="Escultura"><span class="auto-style3" style="color: windowtext; text-underline: none">escultura</span></a><span class="auto-style3">, 
        la&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Orfebrería" title="Orfebrería"><span class="auto-style3" style="color: windowtext; text-underline: none">orfebrería</span></a><span class="auto-style3">&nbsp;y la&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Metalurgia" title="Metalurgia"><span class="auto-style3" style="color: windowtext; text-underline: none">metalurgia</span></a><span class="auto-style3">&nbsp;del cobre, tejían el algodón y la fibra de agave, desarrollaron el más completo sistema de&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Escritura" title="Escritura"><span class="auto-style3" style="color: windowtext; text-underline: none">escritura</span></a><span class="auto-style3">&nbsp;en América prehispánica, entre los deportes que practicaban se destaca el&nbsp;</span><a class="auto-style2" href="https://es.wikipedia.org/wiki/Juego_de_pelota_mesoamericano" title="Juego de pelota mesoamericano"><span class="auto-style3" style="color: windowtext; text-underline: none">juego 
        de pelota</span></a><span class="auto-style3">, el cual más que un juego era una ceremonia.</span><o:p></o:p></p>
    <p>
        &nbsp;</p>
    <p class="MsoNormal">
        <span class="auto-style3">A pesar de su relativamente pequeña extensión territorial, Guatemala tiene una gran variedad climática, producto de su relieve montañoso que va desde el nivel del mar hasta los 4220 metros sobre ese nivel.</span><a href="https://es.wikipedia.org/wiki/Guatemala#cite_note-8"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">8</span></a><span class="auto-style3">​ Esto propicia que en el país existan ecosistemas tan variados que van desde los&nbsp;</span><a href="https://es.wikipedia.org/wiki/Manglar" title="Manglar"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">manglares</span></a><span class="auto-style3">&nbsp;de los humedales del Pacífico hasta los bosques nublados de alta montaña. Limita al oeste y al norte con&nbsp;</span><a href="https://es.wikipedia.org/wiki/México" title="México"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:
none">México</span></a><span class="auto-style3">, al este con&nbsp;</span><a href="https://es.wikipedia.org/wiki/Belice" title="Belice"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">Belice</span></a><span class="auto-style3">, el&nbsp;</span><a href="https://es.wikipedia.org/wiki/Golfo_de_Honduras" title="Golfo de Honduras"><span class="auto-style3" style="color:windowtext;text-decoration:none;
text-underline:none">golfo de Honduras</span></a><span class="auto-style3">&nbsp;(</span><a href="https://es.wikipedia.org/wiki/Mar_Caribe" title="Mar Caribe"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">mar Caribe</span></a><span class="auto-style3">) y la&nbsp;</span><a href="https://es.wikipedia.org/wiki/Honduras" title="Honduras"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">República de Honduras</span></a><span class="auto-style3">, al sureste con&nbsp;</span><a href="https://es.wikipedia.org/wiki/El_Salvador" title="El Salvador"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">El Salvador</span></a><span class="auto-style3">, y al sur con el&nbsp;</span><a href="https://es.wikipedia.org/wiki/Océano_Pacífico" title="Océano Pacífico"><span class="auto-style3" style="color:windowtext;text-decoration:none;
text-underline:none">océano Pacífico</span></a><span class="auto-style3">. El país posee una superficie de 108&nbsp;889&nbsp;</span><a href="https://es.wikipedia.org/wiki/Km²" title="Km²"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:
none">km²</span></a>.<a href="https://es.wikipedia.org/wiki/Guatemala#cite_note-superficie-2"><span class="auto-style3" style="color:windowtext;text-decoration:none;text-underline:none">2</span></a><span class="auto-style3">​ Su capital es la&nbsp;</span><a href="https://es.wikipedia.org/wiki/Ciudad_de_Guatemala" title="Ciudad de Guatemala"><span class="auto-style3" style="color:windowtext;text-decoration:none;
text-underline:none">Ciudad de Guatemala</span></a><span class="auto-style3">, llamada oficialmente «Nueva Guatemala de la Asunción».</span><o:p></o:p></p>
</body>
</html>
