﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="agregar-admin.aspx.cs" Inherits="WebApplication3.Website.Admin.agregar_admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        html{
            background: url(img/fondo.jpg); 
            width: 100%;
            height: 100%;
        }
        body{
            margin-top: 30px;
        }
        h1{
           text-align: center;
           font-size: 40px;
        }
        #cuerpo{
            text-align: center;
            font-family: 'Times New Roman', Times, serif;
            font-size: 25px;
        }
        #Button1{
            font-size: 20px;
        }
        #header {
				margin:auto;
				width:750px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 50px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
			

        h1{
            text-align: center;
        }

        </style>
</head>
<body>
    <form id="form1" runat="server">
       <div id="header">
			<ul class="nav">
				<li><a href="">Inicio</a></li>
				<li><a href="">Tu Sesión</a>
					<ul>
						<li><a href="">Empresas solicitantes</a></li>
						<li><a href="">Empresas aceptadas</a></li>
						<li><a >Agregar</a>
							<ul>
								<li><a href="agregar-agente.aspx">Agente Turístico</a></li>
								<li><a href="agregar-admin.aspx">Administrador</a></li>
								<li><a href="agregar-tecnico.aspx">Técnico</a></li>
							</ul>
						</li>
                        <li><a >Ver</a>
							<ul>
								<li><a href="ver-agentes.aspx">Agentes Turísticos</a></li>
								<li><a href="ver-admin.aspx">Administradores</a></li>
								<li><a href="ver-tecnicos.aspx">Técnicos</a></li>
							</ul>
						</li>

					</ul>
				</li>
				<li><a href="">Acerca de</a></li>
				<li><a href="../inicio.aspx">Cerrar Sesión</a></li>
			</ul>
		</div>
        <br />
        <br />
        <br />


        <h1>&nbsp;Ingreso de nuevos Administradores</h1>
        <p>&nbsp;</p> <br />

      <div id="cuerpo">
          DPI&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
          <br />
        <br />
        Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
          <br />
        <br />
        Usuario&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
          <br />
        <br />
        Contraseña&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
          <br />
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Agregar Administrador" Height="53px" Width="216px" />
      </div>
    </form>
</body>
</html>
