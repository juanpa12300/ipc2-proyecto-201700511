﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace WebApplication3.Website
{
    public partial class agregar_tecnico : System.Web.UI.Page
    {
        MySqlConnection conection = new MySqlConnection("server = localhost; Uid = root; password = 'jp'; database = fase2");


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            conection.Open();
            String codigo = Convert.ToString(TextBox1.Text);
            String nombre = Convert.ToString(TextBox2.Text);
            String region = Convert.ToString(TextBox3.Text);
            String user = Convert.ToString(TextBox4.Text);
            String pass = Convert.ToString(TextBox5.Text);

            //MySqlCommand a = new MySqlCommand("INSERT INTO tecnico " +
            //    "VALUES(" +
            //        "@codigo," +
            //        "@nombre," +
            //        "@region," +
            //        "@user," +
            //        "@pass" +
            //    ")", conection);

            //a.ExecuteNonQuery();
            //a.Parameters.Add("@codigo", pass);

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conection;
            cmd.CommandText = "INSERT INTO tecnico(codigo,nombre,region,user,pass) VALUES(?,?,?,?,?)";
            //cmd.Prepare();

            cmd.Parameters.Add("codigo", MySqlDbType.VarChar).Value = codigo;
            cmd.Parameters.Add("nombre", MySqlDbType.VarChar).Value = nombre;
            cmd.Parameters.Add("region", MySqlDbType.VarChar).Value = region;
            cmd.Parameters.Add("user", MySqlDbType.VarChar).Value = user;
            cmd.Parameters.Add("pass", MySqlDbType.VarChar).Value = pass;
            cmd.ExecuteNonQuery();

            conection.Close();

        }
    }
}