﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="asignar.aspx.cs" Inherits="WebApplication3.Website.agente.asignar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

         html {
            background: url(/img/gt2.jpg); 
            width: 100%;
            height: 100%;
         }
         body{
            margin-top: 20px;
        }

			
			#header {
				margin:auto;
				width:750px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 50px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
			

        h1{
            text-align: center;
        }

        .auto-style1 {
            font-size: 15pt;
        }
        .auto-style2 {
            text-align: center;
        }

        .auto-style3 {
            font-size: 20pt;
            margin-left: 125px;
            color: #669999;
        }

        .auto-style4 {
            font-size: 20pt;
            margin-left: 125px;
            color: #33CC33;
        }

        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style2">
        <div id="header">
			<ul class="nav">
				<li><a href="">Inicio</a></li>
				<li><a href="">Tu Sesión</a>
					<ul>
						<li><a href="">Empresas solicitantes</a></li>
						<li><a href="">Empresas aceptadas</a></li>
					</ul>
				</li>
				<li><a href="">Acerca de</a></li>
				<li><a href="../inicio.aspx">Cerrar Sesión</a></li>
			</ul>
		</div>
        <br />

        <br />

    <h1>
        Asignar una empresa con tecnicos</h1>

            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                <span class="auto-style1">Codigo de Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                <asp:TextBox ID="TextBox1" runat="server" CssClass="auto-style1"></asp:TextBox>
                <span class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                <asp:TextBox ID="TextBox3" runat="server" CssClass="auto-style1" ReadOnly="True" Width="307px"></asp:TextBox>
            </p>
            <p>
                <span class="auto-style1">Codigo de tecnico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                <asp:TextBox ID="TextBox2" runat="server" CssClass="auto-style1"></asp:TextBox>
                <span class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                <asp:TextBox ID="TextBox4" runat="server" CssClass="auto-style1" ReadOnly="True" Width="307px"></asp:TextBox>
            </p>
            <p>
                &nbsp;</p>
            <asp:Button ID="Button2" runat="server" Height="44px" Text="Verificar" Width="288px" CssClass="auto-style3" OnClick="Button2_Click" />
        &nbsp;<asp:Button ID="Button1" runat="server" Height="44px" Text="Asignar" Width="288px" CssClass="auto-style4" Visible="False" />
        </div>

    </form>

    </body>
</html>
