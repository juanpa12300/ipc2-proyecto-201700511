﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="restaurantes.aspx.cs" Inherits="WebApplication3.Website.restaurantes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    #header {
				margin:auto;
				width: 1050px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 40px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
            h1{
                text-align: center;
            }
            #cuerpo{
                text-align: center;
                font-size: 25px;
            }
            #Button1{
                text-align: center;
            }
        </style>
</head>
<body>
     <form id="form1" runat="server">
        <div id="header">
			<ul class="nav">
				<li><a href="inicio.aspx">Inicio</a></li>
                <li><a href="galeria.aspx">Lugares de Turismo</a></li>
                <li><a href="Admin/admin.aspx">Administrador</a></li>
                <li><a href="">Empleados</a>
                    <ul>
						<li><a href="tecnico/tecnico.aspx">Técnico</a></li>
						<li><a href="agente/agente.aspx">Agente Turístico</a></li>
					</ul>
                </li>
				<li><a href="">Formularios</a>
					<ul>
						<li><a href="restaurantes.aspx">Restaurantes</a></li>
						<li><a href="hoteles.aspx">Hoteles</a></li>
						<li><a href="museos.aspx">Museos</a></li>
					</ul>
				</li>

				<li><a href="inicio.aspx">Acerca de</a></li>
			</ul>
		</div>
        <br />
        <br />
        <br />
        <br />

    <h1>Formulario para Restaurantes</h1>
        <p>&nbsp;</p>

        <div id="cuerpo"> 
            Nombre&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            Dirección&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            Teléfono&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            E - mail&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />

        </div>
    </form>
</body>
</html>
